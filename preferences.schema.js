module.exports = {
    onlineTime: {
        type: 'number',
        title: 'Время онлайна',
        default: 3600000
    },

    authorization: {
        type: "object",
        title: "Авторизация",

        properties: {
            private_key: {
                type: 'string',
                title: 'Ключ шифрования',
                required: true
            },

            oauth: {
                type: 'object',
                title: 'Запрос авторизации',

                properties: {
                    client_id: {
                        type: 'number',
                        title: 'ID приложения'
                    }
                }
            },

            token: {

                type: 'object',
                title: 'Получение токена',

                properties: {
                    client_id: {
                        type: 'number',
                        title: 'ID приложения'
                    },
                    client_secret: {
                        type: 'string',
                        title: 'Секретный ключ'
                    }
                }
            }
        },
    },

    payments: {
        type: 'object',
        title: 'Платёжная система',

        properties: {
            shop: { type: 'number', title: 'ID магазина', required: true },
            secret: { type: 'string', title: 'Секретный ключ', required: true }
        }
    },

    roulette: {

        type: 'object',
        title: 'Рулетка',

        properties: {
            price: {
                type: 'number',
                title: 'Цена рулетки',
                default: 59
            }
        }
    },

    decorative: {

        type: 'object',
        title: 'Декорации',

        properties: {

            backgroundImage: {

                type: 'string',
                title: 'Фоновое изображение'
            }
        }
    },
}