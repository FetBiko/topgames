

var splitSame = ( arr, condition ) => {

    var temp = {};

    arr.forEach( v => {

        var id = typeof condition == 'function'
            ? condition( v )
            : v;

        if( typeof temp[ id ] === 'undefined' )
            temp[ id ] = [];

        temp[ id ].push( v );
    });

    var splittedArr = Object.keys( temp ).map( k => {
        return temp[ k ];
    })

    return splittedArr;
}

module.exports = splitSame;
