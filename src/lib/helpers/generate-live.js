const Live = require( '../../models/live' );
const Game = require( '../../models/game' );
const User = require( '../../models/user' );

const randomName = require( '../../lib/helpers/random-name' );
const assing = require( 'object-assign-deep' );

const generateLive = async ( opts =  {} ) => {

    let _opts = assing( {
        fake: true,
        blacklist: false,
        user: false,
        game: false,
        fakeAfter: true,
        delay: 1000
    }, opts );

    var doc = {};

    if( _opts.fake ) {

        let games = await Game.find( { id: { $nin: ( _opts.blacklist || [] )  }, disabled: false } );

        // С каким-то шансом может выпасть ТОЛЬКО РЕДКАЯ игра.
        if( Math.random() > preferences.live.rareChance )
            games = games.filter( game => game.dropChance <= preferences.live.rarePercent );

        if( games.length == 0 )
            return console.error( 'Не удалось создать фейковый элемент в эфире выигрышей: ', 'ни одна игра не подходит!' );

        doc.game = games[ Math.floor( Math.random() * games.length ) ]._id;
        doc.user = randomName.getFullName();
    } else {

        if( !_opts.user || isNaN( _opts.game ) )
            return console.error( 'Не удалось создать реальный элемент в эфире выигрышей: ', 'параметр \'user\' или \'game\' имеет неверное значение!' );

        let game = await Game.findOne( { id: _opts.game } );

        let user = _opts.user;

        if( typeof user !== 'string' ) {

            user = await User.findOne( { id: _opts.user } );
            
            user = user ? `${user.firstName} ${user.lastName}` : 'Имя скрыто';
        }

        doc.game = game._id;
        doc.user = user;

        if( !game || !user )
            return console.error( 'Не удалось создать реальный элемент в эфире выигрышей: ', `неизвестная игра (${_opts.game}) или пользователь (${_opts.user})!` );

        if( _opts.fakeAfter ) {

            console.log( _opts.delay );
            setTimeout( () => { generateLive( { blacklist: [ _opts.game ], delay: 500 }) }, _opts.delay );
        }
    }

    console.log( 'Элемент', doc );
    var result = await Live.create( doc ).catch( error => console.error( 'error' ) );

    console.log( 'Создан новый элемент в эфире выигрышей!', result.toObject() );
}

module.exports = generateLive;