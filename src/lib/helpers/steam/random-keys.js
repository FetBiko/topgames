const WORDS = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "V", "X", "Y", "Z", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9"];

module.exports = ( count = 0, separator = '\n' ) => {

    let keys = [];

    for( var a = 0; a < count; a++ ) {

        var key = [];

        for( var b = 0; b < 3; b++ ) {
            
            var part = '';

            for( var c = 0; c < 5; c++ )
                part += WORDS[ Math.floor( Math.random() * WORDS.length ) ];

            key.push( part );
        }

        keys.push( key.join( '-' ) );
    }

    return keys.join( separator );
};