module.exports = ( router ) => {
    return '    ' + router.stack
        .filter( v => v.methods.length > 0 )
        .map( i => '[' + i.methods
            .filter( v => v != 'HEAD' )
            .join( '|' ) + '] ' + i.path )
        .join( '\n    ' )
        .split( '[ACL|BIND|CHECKOUT|CONNECT|COPY|DELETE|GET|LINK|LOCK|M-SEARCH|MERGE|MKACTIVITY|MKCALENDAR|MKCOL|MOVE|NOTIFY|OPTIONS|PATCH|POST|PROPFIND|PROPPATCH|PURGE|PUT|REBIND|REPORT|SEARCH|SUBSCRIBE|TRACE|UNBIND|UNLINK|UNLOCK|UNSUBSCRIBE]' )
        .join( '[ALL]' );
}