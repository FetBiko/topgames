
const Game = require( '../../models/game' );
const Promocode = require( '../../models/promocode' );
const Pack = require( '../../models/pack' );
const User = require( '../../models/user' );
const Key = require( '../../models/key' );
const Win = require( '../../models/win' );

const generateLive = require( '../../lib/helpers/generate-live' )
const rollPrice = multiplier => {

    let price = parseInt( preferences.roulette.price );
    let multiplierPrice = parseInt( preferences.roulette.multipliers[ multiplier ] );

    if( isNaN( price ) )
        price = 59;
    if( isNaN( multiplierPrice ) )
        multiplierPrice = 0;

    console.log( price, multiplierPrice );

    return price + multiplierPrice;
}

module.exports = class RouletteController {

    static async roll( $ ) {

        var parameters = $.request.body,
            price = rollPrice( parameters.multiplier );

        if( !$.user )  return $.body = { error: 'NEED_AUTH' };
        if( $.user.balance < price ) return $.body = { error: 'LOW_BALANCE', balance: $.user.balance };
        
        // if( !$.user ) $.user = { id: 154843561 }

        // >> Load all active games.
        var games = await Game.find( { disabled: false, dropChance: { $gt: 0 } }, 'id dropChance -_id' );
        console.log( 'total games', games.length );

        // >> Load keys count for every game and return updated object.
        var promises = games.map( async game => {

            var keysCount = await Key.countDocuments( { 
                game: game.id, 
                user: { $exists: false }, 
                win: { $exists: false }, 
                value: { $exists: true }
            } );

            return {
                id: game.id,
                dropChance: game.dropChance,
                keysCount: keysCount || 0
            }
        });

        games = await Promise.all( promises );
        console.log( 'loaded', games );

        // >> Filter games.
        games = games.filter( game => game.keysCount > 0 );
        console.log( 'filtered', games );

        // >> Select random game.
        const totalWeight = games.reduce( ( prev, cur ) => ( parseInt( prev ) || 0  ) + cur.dropChance  );

        const range = ( min, max ) => Math.random() * ( max - min ) + min;

        const getRandomGame = () => {

            let randomWeight = range( 0, totalWeight );
            let weightSum = 0;

            for( let i = 0; i < games.length; i++) {

                weightSum += games[i].dropChance;
                weightSum = +weightSum.toFixed(2);
    
                if( randomWeight <= weightSum ) 
                    return games[i];
            }
        }

        var randomResult = getRandomGame();

        const game = await Game.findOne( { id: randomResult.id }, '-_id id title steamPrice sellPrice image' );

        const win = await Win.create( {
            game: game.id,
            user: $.user.id 
        });

        const updatedUser = await User.findOneAndUpdate( 
            { id: $.user.id },
            { balance: $.user.balance - price },
            { new: true } );

        $.body = { 
            win: win.id, 
            balance: updatedUser.balance,
            game,
            success: true
        }

        generateLive( {
            fake: false,
            user: `${updatedUser.firstName} ${updatedUser.lastName}`,
            game: game.id,
            delay: Math.round( preferences.live.fakeAfterDelay + Math.random() ) * 1000
        })
    }

    static async items( $ ) {

        var games = await Game.find( { disabled: false }, '-_id id title image steamPrice' );

        games.sort( ( a, b ) => a.steamPrice > b.steamPrice );
        
        $.body = { games }
    }

    static async price( $ ) {

        var parameters = $.request.body;

        $.body = { price: rollPrice( parameters.multiplier ) }
    }

    static async win( $ ) {

        var parameters = $.request.body;

        let win = await Win.findOne( { id: parameters.id }, '-_id id user game key locked' );

        if( !win )
            return $.body = { error: 'UNKNOWN_WIN' }

        if( !$.user || win.user !== $.user.id )
            return $.body = { error: 'ACCESS_DENIED', id: parameters.id };

        win = win.toObject();

        if( win.user ) {
            
            const user = await User.findOne( { id: win.user }, '-_id id firstName lastName avatar'  );

            win.user = user 
                ? user
                : false;

        }

        if( typeof win.game === 'number' ) {

            const game = await Game.findOne( { id: win.game }, '-_id id title image steamPrice sellPrice'  );

            win.game = game 
                ? game
                : false;

        }

        console.log( win );
        $.body = { win }
    }

    static async activateWin( $ ) {

        var parameters = $.request.body;

        let win = await Win.findOne( { id: parameters.win }, '-_id id locked key user game' );

        if( !win )
            return $.body = { error: 'UNKNOWN_WIN' }

        // $.user = { id: 154843561 };

        if( !$.user || win.user !== $.user.id )
            return $.body = { error: 'ACCESS_DENIED' }

        if( win.locked )
            return $.body = { error: 'WIN_LOCKED' }

        var game = await Game.findOne( { id: win.game }, '-_id id title steamPrice sellPrice image' );
        var user = await User.findOne( { id: win.user }, '-_id id balance' );

        if( parameters.prize == 'key' ) {

            let key = await Key.findOne( { game: win.game, user: { $exists: false }, win: { $exists: false } } );

            win = await Win.findOneAndUpdate( { id: win.id  }, {
                key: key.value,
                locked: true
            }, { new: true } ).select( '-_id id locked key' )

            await Key.findOneAndUpdate( { id: key.id }, {
                win: win.id,
                user: user.id
            });

        } else if( parameters.prize == 'money' ) {
            user = await User.findOneAndUpdate( { id: win.user }, { 
                balance: user.balance + ( game.sellPrice || 1 )
            }, { new: true } ).select( '-_id id balance' );;
            
            win = await Win.findOneAndUpdate( { id: win.id  }, {
                locked: true
            }, { new: true } ).select( '-_id id locked' )

        } else return $.body = { error: 'UNKNOWN_PRIZE' }

        return $.body = {

            win: {
                ...win.toObject(),
                game: game.toObject(),
                user: user.toObject()
            },
            
            balance: user.balance
        }
    }

    static async pack( $ ) {

        var parameters = $.request.body;
        
        // >> Загрузка коробки и проверка данных.
        if( !$.user )
            return $.body = { error: 'NEED_AUTH' };

        let pack = await Pack.findOne( { id: parameters.id } );
        let promocode = await Promocode.findOne( { code: parameters.code }, '-_id code' );
        
        console.log( promocode );

        if( pack.bought >= pack.limit )
            return $.body = { error: 'LIMIT_REACHED' };

        if( !promocode || promocode.code !== parameters.code )
            return $.body = { error: 'INVALID_PROMOCODE' };

        if( $.user.balance < pack.price && !isNaN( pack.price ) )
            return $.body = { error: 'LOW_BALANCE', balance: $.user.balance, price: pack.price };
        
        // >> Load all active games.
        var games = await Game.find( { disabled: false, dropChance: { $gt: 0 } }, 'id dropChance -_id' );

        // >> Load keys count for every game and return updated object.
        var promises = games.map( async game => {

            var keysCount = await Key.countDocuments( { 
                game: game.id, 
                user: { $exists: false }, 
                win: { $exists: false }, 
                value: { $exists: true }
            } );

            return {
                id: game.id,
                dropChance: game.dropChance,
                keysCount: keysCount || 0
            }
        });

        games = await Promise.all( promises );

        // >> Filter games.
        games = games.filter( game => game.keysCount > 0 );

        // >> Select random game.
        const totalWeight = games.reduce( ( prev, cur ) => ( parseInt( prev ) || 0  ) + cur.dropChance  );

        const range = ( min, max ) => Math.random() * ( max - min ) + min;

        const getRandomGame = () => {

            let randomWeight = range( 0, totalWeight );
            let weightSum = 0;

            for( let i = 0; i < games.length; i++) {

                weightSum += games[i].dropChance;
                weightSum = +weightSum.toFixed(2);
    
                if( randomWeight <= weightSum ) 
                    return games[i];
            }
        }

        var randomResult = getRandomGame();

        let game = await Game.findOne( { id: randomResult.id }, '-_id id title steamPrice sellPrice image' );

        let win = await Win.create( {
            game: game.id,
            user: $.user.id 
        });

        // >> Обновление коробки и пользователя.
        pack = await Pack.findOneAndUpdate( 
            { id: pack.id },
            { bought: pack.bought + 1 },
            {
                new: true,
                select: '-_id id bought limit price'
            }
        );
        
        let user = await User.findOneAndUpdate( 
            { id: $.user.id },
            { balance: $.user.balance - pack.price },
            { new: true }
        );

        $.body = { 
            win: win.id, 
            balance: user.balance,
            game,
            pack,
            success: true
        }
    }
}