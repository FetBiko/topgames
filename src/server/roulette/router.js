const router        = require( 'koa-router' )();
const controller    = require( './controller' );

router
    .post( '.roll',     controller.roll )
    .post( '.items',    controller.items )
    .post( '.price',    controller.price )
    .post( '.getWin',    controller.win )
    .post( '.activateWin',    controller.activateWin )
    .post( '.pack',    controller.pack )

module.exports = router;
