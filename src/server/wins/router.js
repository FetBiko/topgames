const router        = require( 'koa-router' )();
const controller    = require( './controller' );

router
    .post( '/',                     controller.getAll )
    .post( '/win/:id',              controller.get )
    .post( '/win/:id/activate',     controller.activate )

module.exports = router;
