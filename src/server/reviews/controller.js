
const Review = require( '../../models/review' );
const User = require( '../../models/user' );

const ago = require( 'node-time-ago'  );

module.exports = class ReviewsController {

    static async index( $ ) {

        var reviews = await Review.find( { state: 'published' } )
            .select( 'id text user likes createdAt' )
            .limit( 20 )
            .sort( { id: -1 } )

        reviews = reviews.map( async review => {

            var user = await User.findOne( { id: review.user }, '-_id id avatar firstName lastName' );
            
            return {
                id: review.id,
                user: user,
                text: review.text,
                time: ago( new Date( review.createdAt ) ),
                likes: {
                    items: review.likes,
                    count: review.likes.length
                },
                liked: $.user ? review.likes.includes( $.user.id ) : undefined, 
            }
        });

        await Promise.all( reviews ).then( result => {

            $.finalRender( 'common/reviews', { reviews: result } );
        });
    }

    static async get( $ ) {

        var params = $.request.body, search = { state: 'published' };

        var parameters = {
            limit: Number( params.count || 25 ),
            skip: Number( params.skip || 0 )
        }

        var total = await Review.countDocuments( search );

        if( parameters.limit === 0 ) {

            return $.body = {
                items: [],
                count: 0,
                total
            }
        }
        
        var reviews = await Review.find( search )
            .sort( { id: -1 } )
            .skip( parameters.skip ) 
            .limit( parameters.limit )
            .select( 'id text user likes createdAt' )

        reviews = reviews.map( async review => {

            var user = await User.findOne( { id: review.user }, '-_id id avatar firstName lastName' );
            
            return {
                id: review.id,
                text: review.text,
                time: new Date( review.createdAt ).getTime(),
                likes: {
                    items: review.likes,
                    count: review.likes.length
                },
                liked: $.user ? review.likes.includes( $.user.id ) : undefined, 
                user: user
            }
        });

        await Promise.all( reviews ).then( result => {
            $.body = { items: result, count: result.length, total: total };
        });
    }

    static async create( $ ) {

        if( !$.user ) return $.body = { error: 'need_auth' };

        var params = $.request.body;

        var parameters = {
            user: $.user.id,
            text: params.text,
            state: [ 'administrator' ].includes( $.user.role ) ? 'published' : 'moderated'
        }

        var review = await Review.create( parameters );

        review = { 
            id: review.id,
            text: review.text,
            user: review.user,
            likes: review.likes,
            time: review.createdAt,
            state: review.state
        }

        review.user = await User.findOne( { id: review.user }, '-_id id avatar firstName lastName' );
        review.liked = $.user ? review.likes.includes( $.user.id ) : undefined;

        $.body = review;
    }

    static async like( $ ) {

        if( !$.user ) return $.body = { error: 'need_auth' };

        var params = $.request.body;

        var search = { id: params.reviewId }

        var review = await Review.findOne( search );

        if( !review )
            return $.body = { error: 'not_found' }

        if( review.likes.includes( $.user.id ) ) {

            var review = await Review.findOneAndUpdate( search, { $pull: { likes: $.user.id } }, { new: true } );
            $.body = { liked: false, total: review.likes.length }
        } else {

            var review = await Review.findOneAndUpdate( search, { $push: { likes: $.user.id } }, { new: true } );
            $.body = { liked: true, total: review.likes.length }
        }
    }
}