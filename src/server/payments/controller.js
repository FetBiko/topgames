const User = require( '../../models/user' );
const Payment = require( '../../models/payment' );

class PaymentsController {

    static async payment( $ ) {

        const parameters = $.request.body;

        // if( !$.user ) $.user = { id: 154843561 }

        let payment = await Payment.create( {
            user: $.user.id,
            amount: parseFloat( parameters.amount ),
            description: 'Платёж пользователя №' + $.user.id 
        } );
        
        if( !payment )
            return $.body = { error: 'INVALID_DATA' }

        payment = await payment.sign();

        console.log( payment.toPrimepayerData );

        return $.finalRender( 'common/payment', { payment: payment.toPrimepayerData } );
    }

    static async completed( $ ) {

        console.log( 'payment completed', $.request.body );

        $.redirect( '/profile' );
    }

    static async canceled( $ ) {

        console.log( 'payment canceled', $.request.body );
        $.redirect( '/profile' );
    }

    static async interaction( $ ) {


        const parameters = $.request.body;

        console.log( 'payment interaction', parameters );

        const payment = await Payment.findOne( { id: parameters.payment } );
        const user = await User.findOne( { id: payment.user } );

        if( !user )
            throw new Error( 'Unknown user.' );

        console.log( parameters );

        // if( !preferences.payments.remote.includes( parameters.remote_addr ) )
        //     throw new Error( 'Unknown remote address.' );

        let payload = {
            shop: parameters.shop,
            payment: parameters.payment,
            currency: parameters.currency,
            description: parameters.description,
            user: parameters.uv_user,
            amount: parameters.amount
        }

        // let payload = parameters;

        if( preferences.payments.currency.toString() !== payload.currency.toString() )
            throw new Error( 'Invalid currency.' );

        if( !payment.validateSignature( parameters, parameters.sign ) )
            throw new Error( 'Invalid signature.' );

        let success = await user.updateOne( { balance: user.balance + payment.amount } );

        if( success && Boolean( success.ok ) ) {

            await payment.updateOne( { completed: 'completed' } );
            return $.status = 200;
        }
        
        $.status = 500;
    }
}

module.exports = PaymentsController;