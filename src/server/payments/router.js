const router        = require( 'koa-router' )();
const controller    = require( './controller' );

router
    .post( '/completed',          controller.completed )
    .post( '/canceled',          controller.canceled )
    .post(  '/payment',         controller.payment )
    .post( '/interaction',       controller.interaction )



module.exports = router;