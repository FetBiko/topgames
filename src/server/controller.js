
const User = require( '../models/user' );
const Game = require( '../models/game' );
const Pack = require( '../models/pack' );
const Live = require( '../models/live' );

const ago = require( 'node-time-ago' );

module.exports = class Controller {

    static async index( $ ) {

        let online = await User.countDocuments( { lastSeen: { $gte: Date.now() - preferences.onlineTime } } );

        let games = await Game.find( { disabled: false }, '-_id id image' );
        let packs = await Pack.find().populate( 'game' );

        let lives = await Live.find( {}, 'user game createdAt' )
            .sort( { _id: -1 } )
            .limit( 12 )
            .populate( 'game', '-_id id title image' )

        lives = lives.map( live => {

            live.ago = ago( live.createdAt );
            return live;
        })

        $.finalRender( 'common/index', { games, packs, lives, online } );
    }

    static async terms( $ ) {

        console.log( 'user', $.user );

        $.finalRender( 'common/terms' );
    }

    static async reviews( $ ) {

        $.finalRender( 'common/reviews' )
    }

    static async online( $ ) {

        const parameters = $.request.body;

        const online = await User.countDocuments( { lastSeen: { $gte: Date.now() - preferences.onlineTime } } );

        let lives = await Live.find( {}, 'user game createdAt' )
            .sort( { _id: -1 } )
            .limit( 12 )
            .populate( 'game', '-_id id title image' )

        lives = lives.map( live => ({
            _id: live._id,
            game: live.game,
            user: live.user,
            ago: ago( live.createdAt )
        }) );

        $.body = { online, lives }
    }
}