
const { combine } = require( '../lib/helpers/router' );

const root = require( 'koa-router' )();
const controller = require( './controller' );

const access = require( '../middleware/access' );

root
    .get( '/', controller.index )
    .get( '/terms', controller.terms )
    .post( '/online', controller.online )

const router = combine( [

    {
        prefix: '/dashboard',
        router: require( './dashboard/router' ),
        middlewares: [ access( 'administrator' ) ]
    },
    {
        prefix: '/auth',
        router: require( './auth/router' ),
    },
    {
        prefix: '/reviews',
        router: require( './reviews/router' )
    },
    {
        prefix: '/roulette',
        router: require( './roulette/router' )
    },
    {
        prefix: '/profile',
        router: require( './profile/router' ),
        middlewares: [ access() ]
    },
    {
        prefix: '/payments',
        router: require( './payments/router' )
    }
], root );

module.exports = router;
