const router        = require( 'koa-router' )();
const controller    = require( './controller' );

// router
//     .param( 'community',    controller.paramCommunity )
const access = require( '../../middleware/access' );

router
    .get( '/login',         controller.login )
    .get( '/logout',        access(), controller.logout )
    .get( '/login/vk',      controller.vk );

module.exports = router;