
const Pack = require( '../../../models/pack' );
const Game = require( '../../../models/game' );
const JSON = require( 'json5' );

module.exports = class PacksController {

    static async index( $ ) {

        const games = await Game.find();

        $.finalRender( 'dashboard/packs/index', { games } );
    }

    static async get( $ ) {

        var parameters = $.request.body, search = {};

        try {
            search = JSON.parse( parameters.search.value );
        } catch( e ) { }

        const packs = await Pack.find( search, '-_id id game limit bought price' )
            .skip( Number( parameters.start ) ) 
            .limit( Number( parameters.length ) )
            .populate( 'game', 'id title' )

        const recordsTotal = await Pack.countDocuments();
        const recordsFiltered = await Pack.countDocuments( search );

        const draw = Number( parameters.draw ) + 1
        
        $.body = {
            
            data: packs.map( pack => pack.toObject() ),

            draw, recordsTotal, recordsFiltered
        }
    }

    static async pack( $ ) {

        var parameters = $.params;

        const currentPack = await Pack.findOne( { id: parameters.id } ).populate( 'game', 'id title' );
        const games = await Game.find();

        $.finalRender( 'dashboard/packs/pack', { currentPack, games } );
    }

    static async create( $ ) {
        
        var parameters = $.request.body;

        let pack = await Pack.create( parameters );

        $.body = { success: !!pack }
    }

    static async edit( $ ) {

        var parameters = $.request.body;

        try {
            var result = await Pack.findOneAndUpdate( { id: parameters.id }, parameters );
            return $.body = { success: true }
        } catch(  error ) {
            return $.body = { success: false }
        }
    }

    static async remove( $ ) {

        var parameters = $.request.body;

        try {
            await Pack.findOneAndDelete( { id: parameters.id } );

            return $.body = { success: true }
        } catch( error ) {
            return $.body = { success: false }
        }
    }


}