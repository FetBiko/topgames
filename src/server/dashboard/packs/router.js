const router        = require( 'koa-router' )();
const controller    = require( './controller' );


router
    .get( '/',      controller.index )
    .post( '/',     controller.get )
    .get( '/:id',     controller.pack )
    .post( '.create',     controller.create )
    .post( '.edit',     controller.edit )
    .post( '.remove',     controller.remove )
    
module.exports = router;
