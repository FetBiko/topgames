const Preferences = require( 'preferences' );
const path = require( 'path' );
const fs = require( 'fs' );
const assign = require( 'object-assign-deep' );

module.exports = class DashboardController {

    static async index( $ ) {

        $.finalRender( 'dashboard/settings/index', {
            preferencesSchema: require( '../../../preferences.schema' )
        } );
    }

    static async updatePrefs( $ ) {

        let parameters = $.request.body;

        try {

            fs.unlinkSync( path.join( __dirname, '.preferences') )

            preferences = new Preferences( 'preferences', assign( preferences, parameters.preferences ), {
                key: 'super-secret-key',
                file: path.join( __dirname, '.preferences'),
                format: 'yaml'
            } );

            preferences.save();

            $.body = { success: true }
        } catch( error ) {
            console.log( error )
            $.body = { error }
        }
    }
}