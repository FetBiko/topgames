const Key = require( '../../../models/key' );
const User = require( '../../../models/user' );
const Game = require( '../../../models/game' );

const JSON = require( 'json5' );

const keyValidate = new RegExp( /[A-Z0-9]{5}-[A-Z0-9]{5}-[A-Z0-9]{5}/i );

module.exports = class DashboardRouletteController {

    static async index( $ ) {

        var games = await Game.find( {}, 'id title' );

        console.log( games );

        $.finalRender( 'dashboard/keys/index', { games } );
    }

    static async get( $ ) {

        var parameters = $.request.body,
            search = {},
            cache = { game: {}, user: {}  }

        try {
            search = JSON.parse( parameters.search.value );
        } catch( e ) { }

        var keys = await Key.find( search, 'id game user win value -_id' )
            .skip( Number( parameters.start ) ) 
            .limit( Number( parameters.limit ) || 25 )

        const recordsTotal = await Key.countDocuments();
        const recordsFiltered = await Key.countDocuments( search );

        const draw = Number( parameters.draw ) + 1

        // console.time( 'load keys info' );

        // SSSSLOOOW!
        // #TODO: Fix slow loading data.
        keys = keys.map( async key =>  {

            var user, game;

            if( cache.game[key.game] ) {

                game = cache.game[key.game]
            } else {
                game = ( cache.game[key.game] = await Game.findOne( { id: key.game }, 'id title' ) );
            }

            if( cache.user[key.user] ) {
                user = cache.user[key.user]
            }  else {
                user = ( cache.user[key.user] = await User.findOne( { id: key.user }, 'id firstName lastName' ) );
            }

            return  {
                id: key.id,
                value: key.value,

                win: key.win || false,

                user: user ? { 
                    id: user.id,
                    firstName: user.firstName,
                    lastName: user.lastName
                } : false,

                game: game ? { 
                    id: game.id,
                    title: game.title
                } : false
            };
        });

        keys = await Promise.all( keys );

        // console.timeEnd( 'load keys info' );

        console.log( keys );

        $.body = {
            data: keys,
            draw, recordsTotal, recordsFiltered
        }
    }

    static async create( $ ) {

        var parameters = $.request.body,
            keys = typeof parameters.keys === 'string' 
                ? parameters.keys.split( '\n' )
                : parameters.keys,
            success = [],
            game = await Game.findOne( { id: parseInt( parameters.game ) }, 'id title -_id' );

        if( !Array.isArray( keys ) )
            return $.body = { success: false, error: 'INVALID_KEYS' };
        if(  !game )
            return $.body = { success: false, error: 'UNKNOWN_GAME' };

        console.log( keys );
        keys = keys
            .filter( key => keyValidate.test( key ) )
            .map( value => ( { game: game.id , value } ) );
            console.log( keys );

        await Promise.all( keys.map( async value => {

            await Key.create( value )
                .then( key => {

                    success.push( key.id );
                }).catch( ( error ) => {
                    console.log( error )
                 } )

        }) ).then( () => {

            $.body = { count: success.length, success: success.sort(), game }
        });
    }

    static async remove( $ ) {
        
        var parameters = $.request.body;

        var ids = Array.isArray( parameters.ids ) 
            ? parameters.ids
            : ( parameters.ids || '' )
                .split( ' ' ).join( '' )    // Remove spaces.
                .split( ',' ) || [];        // Convert to array.

        if( parameters.id && !parameters.ids )     ids.push( parameters.id );

        var result = await Key.remove( { id: { $in: ids } } );

        $.body = { success: result.ok == 1 }
    }
}