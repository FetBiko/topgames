const router        = require( 'koa-router' )();
const controller    = require( './controller' );

router
    .get( '/',              controller.index )
    .post( '.get',          controller.get )
    .post( '.create',          controller.create )
    .post( '.remove',          controller.remove )
    
module.exports = router;