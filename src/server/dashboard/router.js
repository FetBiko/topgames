const { combine } = require( '../../lib/helpers/router' );

const router        = require( 'koa-router' )();
const controller    = require( './controller' );

router.
    get( '/', controller.index )
    .post( '.updatePrefs', controller.updatePrefs )
    

module.exports = combine( [
    {
        prefix: '/games',
        router: require( './games/router' ),
        middlewares: []
    },
    {
        prefix: '/users',
        router: require( './users/router' ),
        middlewares: []
    },

    {
        prefix: '/reviews',
        router: require( './reviews/router' ),
        middlewares: []
    },
    {
        prefix: '/keys',
        router: require( './keys/router' ),
        middlewares: []
    },
    {
        prefix: '/wins',
        router: require( './wins/router' ),
        middlewares: []
    },
    {
        prefix: '/packs',
        router: require( './packs/router' ),
        middlewares: []
    },
    {
        prefix: '/promocodes',
        router: require( './promocodes/router' ),
        middlewares: []
    },
    // {
    //     prefix: '/keys',
    //     router: require( './keys/keys.router' ),
    //     middlewares: []
    // }
], router );