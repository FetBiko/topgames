
const Promocode = require( '../../../models/promocode' );
const Pack = require( '../../../models/pack' );

const JSON = require( 'json5' );

module.exports = class PacksController {

    static async index( $ ) {

        const packs = await Pack.find()
            .populate( 'game', '-_id id title' );

        $.finalRender( 'dashboard/promocodes/index', { packs } );
    }

    static async get( $ ) {

        var parameters = $.request.body, search = {};

        try {
            search = JSON.parse( parameters.search.value );
        } catch( e ) { }

        let promocodes = await Promocode.find( search, '-_id id pack code' )
            .skip( Number( parameters.start ) ) 
            .limit( Number( parameters.length ) )
            .deepPopulate( [ 'pack.game', 'pack' ], {
                populate: {
                    pack: {
                        select: 'id game bought limit'
                    },

                    'pack.game': {
                        select: '-_id id title'
                    }
                }
            } )

            // .populate( 'pack.game' )

        console.log( promocodes );

        const recordsTotal = await Promocode.countDocuments();
        const recordsFiltered = await Promocode.countDocuments( search );

        const draw = Number( parameters.draw ) + 1
        
        $.body = {
            
            data: promocodes.map( promocode => promocode.toObject() ),
            draw, recordsTotal, recordsFiltered
        }
    }

    static async promocode( $ ) {

        var parameters = $.params;

        const currentPromocode = await Promocode.findOne( { id: parameters.id }, '-_id id code pack' )
            .deepPopulate( [ 'pack.game', 'pack' ], {
                populate: {
                    pack: {
                        select: '-_id id game bought limit'
                    },

                    'pack.game': {
                        select: '-_id id title'
                    }
                }
            } )

            const packs = await Pack.find()
                .populate( 'game', '-_id id title' );

        $.finalRender( 'dashboard/promocodes/promocode', { currentPromocode, packs } );
    }

    static async create( $ ) {
        
        var parameters = $.request.body;

        let promocode = await Promocode.create( {
            code: parameters.code,
            pack: parameters.pack
        } );

        $.body = { success: !!promocode }
    }

    static async edit( $ ) {

        var parameters = $.request.body;

        try {
            var result = await Promocode.findOneAndUpdate( { id: parameters.id }, parameters );
            return $.body = { success: true }
        } catch(  error ) {
            return $.body = { success: false }
        }
    }

    static async remove( $ ) {

        var parameters = $.request.body;

        try {
            await Promocode.findOneAndDelete( { id: parameters.id } );

            return $.body = { success: true }
        } catch( error ) {
            return $.body = { success: false }
        }
    }


}