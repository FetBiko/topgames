const router        = require( 'koa-router' )();
const controller    = require( './controller' );


router
    .get( '/',      controller.index )
    .post( '/',     controller.get )
    .get( '/:id',     controller.promocode )
    .post( '.create',     controller.create )
    .post( '.edit',     controller.edit )
    .post( '.remove',     controller.remove )
    
module.exports = router;
