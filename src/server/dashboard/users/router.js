const router        = require( 'koa-router' )();
const controller    = require( './controller' );

// router
//     .param( 'community',    controller.paramCommunity )

router
    .get( '/',          controller.index )
    .get( '/:id',       controller.profile )
    .post( '/',         controller.get  )
    .post( '.update',   controller.update )
    
module.exports = router;
// module.exports = combine( [
//     {
//         prefix: '/games',
//         router: require( './games/games.router' ),
//         middlewares: []
//     },
//     {
//         prefix: '/users',
//         router: require( './users/users.router' ),
//         middlewares: []
//     },
//     {
//         prefix: '/keys',
//         router: require( './keys/keys.router' ),
//         middlewares: []
//     }
// ], router );