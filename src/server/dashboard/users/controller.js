
const User = require( '../../../models/user' );
const JSON = require( 'json5' );

module.exports = class DashboardUsersController {

    static async index( $ ) {

        const users = await User.find();
        $.finalRender( 'dashboard/users/index', {
            users
        } );
    }

    static async get( $ ) {

        var parameters = $.request.body, search = {};

        try {
            search = JSON.parse( parameters.search.value );
        } catch( e ) { }

        const users = await User.find( search, 'id firstName lastName role balance isTwisted' )
            .skip( Number( parameters.start ) ) 
            .limit( Number( parameters.limit ) || 25 )

        const recordsTotal = await User.countDocuments();
        const recordsFiltered = await User.countDocuments( search );

        const draw = Number( parameters.draw ) + 1
        
        $.body = {
            
            data: users.map( user => ( {
                id: user.id,
                name: `${user.firstName} ${user.lastName}`,
                role: user.role,
                balance: user.balance,
                isTwisted: user.isTwisted
            }) ),

            draw, recordsTotal, recordsFiltered
        }
    }

    static async profile( $ ) {

        var parameters = $.params;

        const currentUser = await User.findOne( { id: parameters.id } );


        $.finalRender( 'dashboard/users/profile', { currentUser } )
    }

    static async update( $ ) {

        var parameters = $.request.body,
            update = {
                balance: Number( parameters.balance ),
                role: parameters.role,
                isTwisted: parameters.isTwisted
            }

        try {
            const result = await User.findOneAndUpdate( { id: parameters.id }, update, { new: true } ).catch( console.error );

            $.body = { success: result.state == parameters.state }
        } catch( e ) {
            $.body = { success: false }
        } 
    }
}