
const Review = require( '../../../models/review' );
const ago = require( 'node-time-ago' );

module.exports = class DashboardReviewsController {

    static async index( $ ) {

        let transformData = reviews => {

            return reviews.map( ( review ) => ({
                text: review.text,
                user: review.user,
                id: review.id,
                time: ago( new Date( review.createdAt ) )
            }));
        }

        const states = [ 'moderated', 'published', 'hidden' ];

        var promises = states.map( state => new Promise( ( resolve, reject ) => {

            Review.find( { state }, 'user text createdAt id -_id'  ).then( result => {

                resolve( { state: state, data: transformData( result ) } );
            }).catch( console.error );
        }));

        var data = await Promise.all( promises ), reviews = {};

        data.forEach( item => {
            reviews[item.state] = item.data; });

        $.finalRender( 'dashboard/reviews/index', {
            reviews });
    }

    static async state( $ ) {

        const parameters = $.request.body;
        try {
            const result = await Review.findOneAndUpdate( { id: parameters.id }, { state: parameters.state }, { new: true } ).catch( console.error );
            $.body = { success: result.state == parameters.state, state: result.state  }
        } catch( e ) {
            $.body = { success: false }
        }
    }
}