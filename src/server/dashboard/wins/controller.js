
const Win = require( '../../../models/win' );
const JSON = require( 'json5' );

module.exports = class DashboardUsersController {

    static async index( $ ) {

        $.finalRender( 'dashboard/wins/index' );
    }

    static async get( $ ) {

        var parameters = $.request.body, search = {};

        try {
            search = JSON.parse( parameters.search.value );
        } catch( e ) { }

        const wins = await Game.find( search, 'id title steamPrice sellPrice dropChance disabled' )
            .skip( Number( parameters.start ) ) 
            .limit( Number( parameters.length ) )

        const recordsTotal = await Game.countDocuments();
        const recordsFiltered = await Game.countDocuments( search );

        const draw = Number( parameters.draw ) + 1
        
        $.body = {
            
            data: games.map( game => ( {
                id: game.id,
                title: game.title,
                steamPrice: game.steamPrice,
                sellPrice: game.sellPrice,
                dropChance: game.dropChance,
                disabled: game.disabled
            }) ),

            draw, recordsTotal, recordsFiltered
        }
    }

    static async win( $ ) {

        var parameters = $.params;

        const currentWin = await Win.findOne( { id: parameters.id } );

        $.finalRender( 'dashboard/wins/win', { currentWin } );
    }

}