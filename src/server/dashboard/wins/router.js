const router        = require( 'koa-router' )();
const controller    = require( './controller' );


router
    .get( '/',      controller.index )
    .get( '/:id',   controller.win )

    .post( '.get',   controller.get )
    
module.exports = router;
