
const Game = require( '../../../models/game' );
const JSON = require( 'json5' );
const multer = require('koa-multer');

const path = require( 'path' )
const crypto = require( 'crypto' );
const mime = require( 'mime' );

const storage = multer.diskStorage( {
    destination: path.join( __rootdir, '/public/uploads' ),
    filename: function( $, file, cb ) {

        crypto.pseudoRandomBytes( 8, function ( err, raw ) {
            cb( null, raw.toString('hex') + Date.now() + '.' + mime.getExtension( file.mimetype ) );
        });
    }
});

const upload = multer( { 
    dest:  path.join( __rootdir, '/public/uploads' ),
    storage,
    limits: {
        fileSize: 1000000 * 2.5,
        files: 1,
        
    },
    fileFilter: function( $, file, cb ) {

        var filetypes = /jpeg|jpg|png/;
        var mimetype = filetypes.test( file.mimetype );
        var extname = filetypes.test( path.extname( file.originalname ).toLowerCase()  );
    
        if (mimetype && extname)
          return cb( null, true );

        var error = new Error( "Error: File upload only supports the following filetypes - " + filetypes );
            error.code = 'WRONG_FILE_EXTENSION';

        cb( error, false );
    }
}).single( 'image' );

module.exports = class DashboardUsersController {

    static async index( $ ) {

        $.finalRender( 'dashboard/games/index' );
    }

    static async get( $ ) {

        var parameters = $.request.body, search = {};

        try {
            search = JSON.parse( parameters.search.value );
        } catch( e ) { }

        const games = await Game.find( search, 'id title steamPrice sellPrice dropChance disabled' )
            .skip( Number( parameters.start ) ) 
            .limit( Number( parameters.length ) )

        const recordsTotal = await Game.countDocuments();
        const recordsFiltered = await Game.countDocuments( search );

        const draw = Number( parameters.draw ) + 1
        
        $.body = {
            
            data: games.map( game => ( {
                id: game.id,
                title: game.title,
                steamPrice: game.steamPrice,
                sellPrice: game.sellPrice,
                dropChance: game.dropChance,
                disabled: game.disabled
            }) ),

            draw, recordsTotal, recordsFiltered
        }
    }

    static async game( $ ) {

        var parameters = $.params;

        const currentGame = await Game.findOne( { id: parameters.id } );

        console.log( currentGame );

        $.finalRender( 'dashboard/games/game', { currentGame } );
    }

    static async update( $ ) {

        var parameters = $.request.body,
            update = {
                id: parameters.id,
                title: parameters.title,
                image: parameters.image,
                steamPrice: parameters.steamPrice,
                sellPrice: parameters.sellPrice,
                dropChance: parameters.dropChance,
                disabled: parameters.disabled
            }

        try {
            const result = await Game.findOneAndUpdate( { id: parameters.id }, update, { new: true } ).catch( console.error );

            $.body = { success: result.state == parameters.state }
        } catch( e ) {
            $.body = { success: false }
        } 
    }

    static async create( $ ) {

        var parameters = $.request.body,
            docs = {
                title: parameters.title,
                image: parameters.image,
                steamPrice: parseInt( parameters.steamPrice ),
                sellPrice: parseInt( parameters.sellPrice ),
                dropChance: parseInt( parameters.dropChance ),
                disabled: parameters.disabled
            }

        try {
            const  result = await Game.create( docs ).catch( console.error );
            $.body = { success: result.id }
        } catch( e ) {
            $.body = { success: false }
        }
    }

    static async delete( $ ) {
        var parameters = $.request.body;

        try {
            const result = await Game.deleteOne( { id: parameters.id } ).catch( console.error );
            $.body = { success: true }
        } catch( e ) {
            $.body = { success: false }
        }
    }
    
    static async uploadImage( $ ) {

        try {
            var result = await upload( $ );

            $.body = {
                // url: new URL( '/uploads/' + $.req.file.filename, config.get( 'homeUrl' ) ) }
                url: '/uploads/' + $.req.file.filename }

        } catch( error ) {
            
            $.body = { error: error.code }
        }   
    }
}