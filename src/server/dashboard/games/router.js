const router        = require( 'koa-router' )();
const controller    = require( './controller' );


router
    .get( '/',          controller.index )
    .get( '/:id',       controller.game )
    .post( '/',         controller.get  )
    .post( '.update',   controller.update )
    .post( '.create',   controller.create )
    .post( '.delete',   controller.delete )
    .post( '.uploadImage',  controller.uploadImage )
    
module.exports = router;
