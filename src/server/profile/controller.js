const User = require( '../../models/user' );
const Win = require( '../../models/win' );
const Game = require( '../../models/game' );

module.exports = class ReviewsController {

    static async index( $ ) {

        $.assert(  $.user, 401, 'Чтобы посмотреть эту страницу, нужно авторизоваться!' );

        let user = await User.findOne( { id: $.user.id } );
        let wins = await Win.find( { user: $.user.id }, '-_id id game locked key' )
            .sort(  { id: -1 } )
            .limit( 12 )

        wins = wins.map( async win => {

            win = win.toObject();
            win.game = await Game.findOne( { id: win.game }, '-_id image title sellPrice' );
            return win;
        });

        wins = await Promise.all( wins );


        console.log( wins );

        $.finalRender( 'common/profile', { user, wins } );
    }

    static async wins( $ )  {

        const parameters = $.request.body;

        if( !$.user )
            return $.redirect( '/' );

        let total = await Win.countDocuments( { user: $.user.id } );

        let wins = await Win.find( { user: $.user.id }, '-_id id game locked key' )
            .sort( { id: -1 } )
            .skip( isNaN( parameters.skip ) ? 12 : Number( parameters.skip ) )
            .limit( isNaN( parameters.count ) ? 12 : Number( parameters.count ) )
            

        wins = wins.map( async win => {

            win = win.toObject();
            win.game = await Game.findOne( { id: win.game }, '-_id image title sellPrice' );
            return win;
        });

        wins = await Promise.all( wins );
        // console.log( wins );
        $.body = { wins, total }
    }
}