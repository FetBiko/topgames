const router        = require( 'koa-router' )();
const controller    = require( './controller' );

router
    .get( '/', controller.index )
    .post( '/wins', controller.wins )

module.exports = router;
