
module.exports = ( opts = {} ) => {

    return async ( $, next ) => {
        console.log( 'handler!' );

        try {
            await next();

            if( ( $.status || 404 ) === 404 ) {

                let error = new Error( 'Странина не найдена!' );
                    error.status = 404;
                    error.title = 'Страница не найдена!';

                $.throw( error )
            }
        } catch( error ) {

            console.log( error );
            if( opts.json )
                return $.body = { error };

            $.finalRender( 'common/error', { error } )
        }
    }
}