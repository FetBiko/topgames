
module.exports = ( conditions = [] ) => {

    return async ( $, next ) => {
        
        if( !Array.isArray( conditions ) )
            conditions = [conditions];

        conditions.forEach( cond => {

            let condition = null;

            if( typeof cond === 'number' )
                condition = ( user = {} ) => user.id == cond;

            if( typeof cond === 'string' )
                condition = ( user = {} ) => user.role == cond;

            if( cond === undefined )
                condition = user => Boolean( user );


            if( typeof condition === 'function' ) {
                
                if( !condition( $.user ) ) {

                    var error = new Error( 'Тебе нельзя просматривать эту страницу! Что ты тут забыл? С:' );
                        error.code = 504;
                        error.title = 'Доступ ограничен!';

                    $.throw( error );
                }
            }
        });

        await next();
    }
}