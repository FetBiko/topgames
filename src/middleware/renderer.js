
module.exports = () => {

    return async ( $, next ) => {

        console.log( 'renderer' );

        $.finalRender = ( view, options, callback ) => {

            options = Object.assign( { preferences }, options );

            if( $.user )
                options.user = $.user;

            $.render( view, options, callback );
        }

        await next();
    }
}