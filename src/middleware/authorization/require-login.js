module.exports = ( url = '/auth/login' ) => {

    return async ( $, next ) => {
        if ( $.isAuthenticated ) {

            await next();
        } else {
            
            if( url )
                return $.redirect( url );

            $.status = 401
            
            $.body = {
                errors: [{ title: 'Login required', status: 401 }]
            }
        }
    }
}