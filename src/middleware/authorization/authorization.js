const config = require( 'config' );

const jwt = require( 'jsonwebtoken' );
const uuidv4 = require( 'uuid/v4' );
const querystring = require( 'querystring' );

const User = require( '../../models/user' );

var _authRedirect = $ => {

    return ()  => {

        $.redirect( 'https://oauth.vk.com/authorize?' + querystring.stringify( {
            ...preferences.authorization.oauth, 
            redirect_uri: config.get( 'homeUrl' ) + preferences.authorization.path } ) );
    }
}

var _accessToken = token => {
    try {
        
        return jwt.verify( token, config.get( 'authorization.private_key' ) );
    } catch( err ) {
        // console.error( 'at:',  err );

        return false;
    }
}
var _refreshToken = async token => {
    try {
        
        jwt.verify( token, config.get( 'authorization.private_key' ) );
        var user = await User.findOne( { refresh_token: token } );

        return user ? user : false;
    } catch( err ) {

        // console.error( 'rt:',  err );
        return false;
    }
}
var _refreshTokens = $ => {

    
    return async user => {

        var accessToken = jwt.sign( user.JWTData, config.get( 'authorization.private_key' ), { expiresIn: '15m' } );
        var refreshToken = jwt.sign( { rand: uuidv4() },  config.get( 'authorization.private_key' ), { expiresIn: '30d' } );

        user.refreshToken = refreshToken;
        user.accessToken = accessToken;

        $.cookies.set( 'web:access', accessToken, { httpOnly: false, expires: new Date( Date.now() + 1000 * 60 * 60 * 24 * 30 ) } );        
        $.cookies.set( 'web:refresh', refreshToken,  { httpOnly: false, expires: new Date( Date.now() + 1000 * 60 * 60 * 24 * 30 ) } );

        $.user = user;
        
        await user.save();

        
        return user;
    }
}

var _logout = ( $ ) => {

    return () => {
        $.cookies.set( 'web:access', null );
        $.cookies.set( 'web:refresh', null );
    }
}

function updateLastSeen( id ) {

    User.findOneAndUpdate( { id: id }, { lastSeen: Date.now() } ).catch( () => {} );
}

module.exports = () => {

    return async ( $, next ) => {

        $.refreshTokens = _refreshTokens( $ );
        $.logout        = _logout( $ );
        $.authRedirect  = _authRedirect( $ );

        // Пользователь ещё не вошёл.
        if( !$.cookies.get( 'web:access' ) )
            return next();

        $.user = _accessToken( $.cookies.get( 'web:access' ) );

        // console.log( 'auth user, access token:', $.user );
        
        // Если web:access валидный, то пропускаем обновление токенов.

        if( $.user ) {

            $.user = await User.findOne( { id: $.user.id } );
            updateLastSeen( $.user.id );
            return next();
        }

        // Проверяем web:refresh.
        user = await _refreshToken( $.cookies.get( 'web:refresh' ) )

        // Если пользователь получен и web:refresh валидный, то
        if( user ) {
            
            // обновляем токены.
            $.user = await $.refreshTokens( user );
            updateLastSeen( $.user.id );
            return next();
        } else {
            
            console.log( 'Erorr auth.' );

            // иначе перекидываем на авторизацию.
            // $.logout();
            return next();
        }
    }
}