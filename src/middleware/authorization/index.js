module.exports = {
    authorization: require( './authorization' ),
    requireLogin: require( './require-login' )
}