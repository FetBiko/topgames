const root = require( 'koa-router' )();

const { combine, map } = require( './lib/helpers/router' );

const server = require( './server/router' );

const { authorization } = require( './middleware/authorization' );

const router = combine( [
    {
        router: server,
        middlewares: [ authorization() ]
    }
], root );

console.log( 'MAP:\n' + map( router ) );
module.exports = router;