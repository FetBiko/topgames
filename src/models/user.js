const mongoose = require( 'mongoose' );
const Schema = mongoose.Schema;

const userSchema = new Schema({
    
    id: {
        type: Number,
        unique: true,
        index: true
    },

    firstName: String,
    lastName: String,
    avatar: String,

    role: {
        type: String,
        enum: [ 'default', 'administrator', 'streamer' ],
        default: 'default'
    },
    
    balance: {
        type: Number,
        default: 0,
        min: 0
    },

    games: {
        type: Number,
        default: 0
    },

    lastSeen: {
        type: Number,
        default: Date.now
    },

    accessToken: {
        type: String,
        select: false
    },
    refreshToken: {
        type: String,
        select: false
    }
});

userSchema.virtual( 'name' ).get( function() {

    return `${ this.firstName } ${ this.lastName }`
});

userSchema.virtual( 'JWTData' ).get( function() {

    return {
        id:             this.id,
        balance:        this.balance,
        role:           this.role,
    }
});

module.exports = mongoose.model( 'User', userSchema );