const mongoose = require( 'mongoose' );
const autoIncrement = require( 'mongoose-auto-increment' );

const Schema = mongoose.Schema;

const payementSchema = new Schema({
    
    id: {
        type: Number,
        unique: true,
        index: true
    },

    amount: {
        type: Number,
        required: true
    },

    user: { 
        type: Number,
        required: true
    },

    description: {
        type: String,
        required: true
    },

    currency: {
        type: Number,
        default: 3
    },

    status:  {
        type: String,
        enum: [ 'created', 'completed', 'canceled' ],
        default: 'created'
    }
    
}, { timestamps: true } );

payementSchema.plugin( autoIncrement.plugin, { model: 'Payment', field: 'id' } );

payementSchema.virtual( 'toPrimepayerData' ).get( function() {

    console.log( preferences.payements );

    return {
        shop: preferences.payments.shop,
        payment: this.id,
        currency: preferences.payments.currency,
        description: this.description,
        user: this.user,
        amount: this.amount,
        sign: this.sign,
    }
});

payementSchema.virtual( 'signed' ).get( function() {

    return Boolean( this.sign );
});

payementSchema.method( 'sign', async function() {

    let data = this.toPrimepayerData;
    delete data.sign;

    let sign = Object.keys( data ).sort().map( key => data[key] );

    sign.push( preferences.payments.secret );
    sign = sign.join( ':' );

    console.log( sign );

    sign = require( 'crypto' ).createHash('sha256').update( sign ).digest( 'hex' );

    let result = await this.updateOne( { sign } );

    if( result && result.ok == 1 ) {

        this.sign = sign;
    } 
    
    return this; 
});

payementSchema.method( 'validateSignature', function( payload, correctSign ) {
    
    console.log( 'validateSignature', payload, correctSign );

    delete payload.sign;

    let sign = Object.keys( payload ).sort().map( key => payload[key] );
    
    sign.push( preferences.payments.secret );
    sign = sign.join( ':' );
    sign = require( 'crypto' ).createHash('sha256').update( sign ).digest( 'hex' );

    return sign == correctSign;
});

module.exports = mongoose.model( 'Payment', payementSchema );