// 

const mongoose = require( 'mongoose' );
const autoIncrement = require( 'mongoose-auto-increment' );

const Schema = mongoose.Schema;

const keySchema = new Schema({
    
    id: {
        type: Number,
        unique: true,
        index: true
    },

    game: {
        type: Number,
        required: true
    },

    user: {
        type: Number
    },

    win: {
        type: Number
    },

    value: {
        type: String,
        validate: /[A-Z0-9]{5}-[A-Z0-9]{5}-[A-Z0-9]{5}/i,
        required: true,
        unique: true,
        uppercase: true
    }
}, { timestamps: true } );

keySchema.plugin( autoIncrement.plugin, { model: 'key', field: 'id' } )

keySchema.method( 'findByGame', function( game = 0, select, options ) {

    return this.find( { game }, select, options );
});

module.exports = mongoose.model( 'Key', keySchema );