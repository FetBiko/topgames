const mongoose = require( 'mongoose' );
const Schema = mongoose.Schema;

const autoIncrement = require( 'mongoose-auto-increment' );

const gameSchema = new Schema({
    
    id: {
        type: Number,
        required: true,
        index: true
    },

    title: {
        type: String,
        required: true
    },

    image: {
        type: String,
        required: true
    }, 

    steamPrice: {
        type: Number,
        required: true,
    },

    sellPrice: {
        type: Number,
        required: true
    },
    
    dropChance: {
        type: Number,
        default: false,
        min: 0,
        max: 100,
        default: 0
    },

    disabled: {
        type: Boolean,
        default: true
    }

}, { autoIndex: true } );

gameSchema.plugin( autoIncrement.plugin, { model: 'Game', field: 'id' } )

module.exports = mongoose.model( 'Game', gameSchema );