const mongoose = require( 'mongoose' );
const autoIncrement = require( 'mongoose-auto-increment' );

const Schema = mongoose.Schema;
const ObjectId = Schema.Types.ObjectId;

const packSchema = new Schema({
    
    id: {
        type: Number,
        unique: true,
        index: true
    },

    game: {
        type: ObjectId,
        ref: 'Game',
        required: true
    },
    
    limit: {
        type: Number,
        required: true
    },

    bought: {
        type: Number,
        default: 0,
    },

    price:  {
        type: Number,
        required: true,
        default: 199
    }

}, { timestamps: true } );

packSchema.plugin( autoIncrement.plugin, { model: 'Pack', field: 'id' } )


module.exports = mongoose.model( 'Pack', packSchema );