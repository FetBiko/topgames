const mongoose = require( 'mongoose' );
const autoIncrement = require( 'mongoose-auto-increment' );
const deepPopulate = require('mongoose-deep-populate')(mongoose);

const Schema = mongoose.Schema;
const ObjectId = Schema.Types.ObjectId;

const promocodeSchema = new Schema({
    
    id: {
        type: Number,
        unique: true,
        index: true
    },

    pack: {
        type: ObjectId,
        ref: 'Pack',
        required: true
    },

    code: {
        type: String,
        required: true
    }

}, { timestamps: true } );

promocodeSchema.plugin( autoIncrement.plugin, { model: 'Promocode', field: 'id' } )
promocodeSchema.plugin( deepPopulate );

module.exports = mongoose.model( 'Promocode', promocodeSchema );