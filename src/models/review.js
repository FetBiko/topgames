const mongoose = require( 'mongoose' );
const autoIncrement = require( 'mongoose-auto-increment' );

const Schema = mongoose.Schema;

const reviewSchema = new Schema({
    
    id: {
        type: Number,
        unique: true,
        index: true
    },

    user: {
        type: Number,
        required: true
    },

    text: {
        type: String,
        maxlength: 255
    },

    likes: {
        type: [Number]
    },

    state:  {
        type: String,
        enum: [ 'moderated', 'published', 'hidden' ],
        default: 'moderated'

    }

}, { timestamps: true } );

reviewSchema.plugin( autoIncrement.plugin, { model: 'Review', field: 'id' } )


module.exports = mongoose.model( 'Review', reviewSchema );