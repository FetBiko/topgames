const $ = require( 'jquery' );
const modals = require( '../helpers/modals' );
const utils = require( '../helpers/utils' );


$( 'button[name=packActivation]' ).click( function( ev ) {

    var button = $( ev.target );

    var data = {
        id: button.data().id
    };

    data.code = $( 'input[name=packCode' + data.id + ']' ).val();

    console.log( 'promocode', data )

    // Disable button.
    button.prop( 'disabled', true );

    $.ajax( {
        type: 'POST',
        url: '/roulette.pack',
        data: data,
        success: function( result ) {

            button.prop( 'disabled', false );

            if( result.error ) {

                var transformErrors = {
                    INVALID_PROMOCODE: {
                        title: 'Промокод недействителен!',
                    },

                    LIMIT_REACHED: {
                        title: 'Достигнут лимит!',
                        message: 'К сожалению, эту коробку купили максимальное количество раз! Но не расстраивайся, скоро мы загрузим ещё ключей!',
                    },
                    NEED_AUTH:  {
                        title: 'Действие недоступно!',
                        message: 'Чтобы крутить рулетку, нужно авторизоваться на сайте!',
                        buttons: [
                            {
                                text: 'Авторизоваться',
                                primary: true,
                                href: '/auth/login'
                            }
                        ]
                    },
                    LOW_BALANCE: {

                        title: 'Недостаточно денег!',
                        message: 'Увы, но тебе не хватает ' + ( result.price - result.balance ) + '₽, чтобы получить эту игру!',
                        buttons: [
                            {
                                text: 'Пополнить баланс',
                                primary: true,
                                href: '/profile'
                            }
                        ]
                    },
                    DEFAULT: {
                        title: 'Неизвестная ошибка!',
                        message: 'Попробуйте ещё раз! Если ошибка повторяется, обратитесь в поддержку'
                    }
                }

                var error = transformErrors[result.error] || transformErrors.DEFAULT;

                return modals.modalError( error.title, error.message, error.buttons  );
            } else {

                if( result.balance ) 
                    utils.updateBalance( result.balance )

                if( result.pack )
                    $( '#pack_' + data.id + ' .counter__value#limit' ).text( result.pack.limit - result.pack.bought + ' / ' + result.pack.limit );

                modals.modalWin( result.win, result.game );
            }
        },
        error: function() {
            button.prop( 'disabled', false );
        }
    });
});
