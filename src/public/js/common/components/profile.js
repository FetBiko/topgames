const modals = require( '../helpers/modals' );
const $ = require( 'jquery' );

function refreshClickHandles()  {
    $( '.wins-list__item' ).click( function(  el ) {

        var winItem = $( el.currentTarget );

        $.ajax( {
            type: 'POST',
            url: '/roulette.getWin',
            data: winItem.data(),
            success: function( result ) {

                if( result.error ) {
        
                    $( '#roulette_play' ).prop( 'disabled', false );
        
                    var transformErrors = {
                        ACCESS_DENIED:  {
                            title: 'Доступ ограничен!',
                            message: 'У вас недостаточно прав для просмотра выигрыша #' + result.id + '!'
                        },
                        DEFAULT: {
                            title: 'Неизвестная ошибка!',
                            message: 'Попробуйте ещё раз! Если ошибка повторяется, обратитесь в поддержку'
                        }
                    }
        
                    var error = transformErrors[result.error] || transformErrors.DEFAULT;
        
                    return modals.modalError( error.title, error.message, error.buttons  );
                }
        
                modals.modalWin( result.win );
            }
        });
    } );
}

refreshClickHandles();

var winsSkip = window.skip || 0, winsCount = window.skip || 0;
var winsLoading = false;

function moreWins() {

    if( winsLoading )
        return;
    
    winsLoading = true;

    function renderWins( wins ) {

        wins.forEach( function( win ) {

            var prize = '<div class="wins-list__prize ' + ( win.locked ? '' : 'no' ) + '">' + ( win.locked ? ( win.key || win.game.sellPrice + '₽' ) : 'Забрать приз!' ) + '</div></div>';
            $( '.wins-list' ).append( '<div class="wins-list__item" data-id="' + win.id + '"><img class="wins-list__image" src="' + win.game.image + '"><div class="wins-list__content"><div class="wins-list__game-title">' +  win.game.title + '</div>' + prize + '</div>' );
        });
    }

    $( '#winsMore' ).prop( 'disabled', true ).text( '...' );

    $.ajax( {
        type: 'POST',
        url: '/profile/wins',
        data: {
            skip: winsSkip,
            count: 12
        },
        success: function( result ) {
            if( Array.isArray( result.wins ) ) {

                winsSkip += result.wins.length;
                renderWins( result.wins );
                refreshClickHandles();
            }

            $( '#winsMore' ).prop( 'disabled', false ).text( 'Показать больше' );
            winsLoading = false;

            if( result.total == winsSkip ) {
                $( '#winsMore' ).remove();
            }
        },
        error: function() {
            winsLoading = false;
        }
    } );


}

$( '#winsMore' ).click( moreWins );
$( '#createPayment').click( function() {

    var amount = $( '.payment-form input[name=amount]' ).val();

    console.log( amount );
});

$( 'button[name=paymentSubmit]' ).click( function() {

    let data = {
        amount: $( 'input[name=paymentAmount]' ).val()
    }

    $.ajax( {
        type: 'POST',
        url: '/payments/payment',
        data: data,
        success: function( result ) {
            modals.modalPayment( result )
        }
    })
});