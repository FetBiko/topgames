const $ = require( 'jquery' );
const ago = require( 'node-time-ago' );
const modals = require( '../helpers/modals' );

function hasError( result ) {

    if( result.error ) {

        modals.modalError( 'Действие недоступно!', 'Чтобы выполнить это действие, нужно авторизоваться на сайте!', [
            {
                text: 'Вход',
                primary: true,
                href: '/auth/login'
            }
        ] );

        return true;
    }
    return false;
}

function likeReview( reviewId, el ) {
    
    let savedState = { liked: el.attr( 'liked' ) };

    el.attr( 'liked', el.attr( 'liked' ) === 'true' ? false : true );

    let handleResult  = function( result ) {

        if( hasError( result ) ) return;

        if( result.error )
            return el.text( savedState  ? 'Нравится?' : 'Не нравится!' )
        
        el.text( result.liked ? 'Не нравится!' : 'Нравится?' )
    }

    $.ajax( {
        type: 'POST',
        url: '/reviews.like',
        data: { reviewId: reviewId },
        success: handleResult
    })
}

function refreshReviewClicks() {
    $( '.reviews-item__meta a' ).click( function( item ) {

        var el = $( item.currentTarget )
        var data = el.data();

        likeReview( data.reviewid, el );
    });
}

refreshReviewClicks();

function createReview( el ) {

    let handleResult  = function( result ) {

        if( hasError( result ) ) return;

        
        $( 'textarea[name="reviewText"]' ).val( '' );

        if( result.state == 'published' ) {

            renderReview( [result], true );
            modals.modalNotification( 'Отзыв принят и опубликован!' );

        } else {
            modals.modalNotification( 'Отзыв принят!', 'Как только отзыв пройдёт модерацию, он появится на этой странице. <br><br> Мы вынуждены проверять отзывы из-за учащённых случаев мошенничества!' );

        }


        skip++;
    }

    var text = $( 'textarea[name="reviewText"]' ).val();

    if( text.length < 5 || text.length > 255 ) {

        modals.modalError( 'Ошибка отправки отзыва', `Отзыв должен содержать от 5 до 255 символов! В вашем отзыве: ${text.length}` );
        return;
    }
    
    $.ajax( {
        type: 'POST',
        url: '/reviews.create',
        data: { text: text },
        success: handleResult
    });
}

$( '.reviews-form .flat-button[name="submitReview"]').click( function() {

    createReview();
});

function moreReviews() {

    let handleResult = function( result ) {

        var items  = result.items;

        if( result.count < count )
            $( '.reviews .more' ).css( { display: 'none' } );

        renderReview( items );

        skip += count;
    }


    $.ajax( {
        type: 'POST',
        url: '/reviews.get',
        data: {
            skip: skip,
            count: count
        },
        success: handleResult
    });

}

function renderReview( review, before ) {

    var container = $( '.reviews__container' );

    if( !Array.isArray( review ) )
        review = [];

    review = review.map( function( r ) {

        var text = '<div class="reviews-item"><div class="reviews-item__header"><img src=' +
            r.user.avatar +
            '" alt="' +
            r.user.firstName + r.user.lastName +
            '"><span>' +
            r.user.firstName + r.user.lastName +
            '</span></div><div class="reviews-item__body"><span>' +
            r.text +
            '</span></div><div class="reviews-item__meta">' +
            ago( r.time );

        if( r.liked !== undefined )
            text += ' • <a class="like" data-reviewid="' + r.id + '" liked="' +  r.liked + '">' + ( r.liked ? 'Не нравится!' : 'Нравится?') + '</a></div>';

        text += '</div></div>';

        return  text;
    });

    console.log( 'rendered', review )
    
    if( before )
        container.html( review.join( '' ) + container.html() );
    else
        container.html( container.html() + review.join( '' )  );

    refreshReviewClicks();
    
}

$( '.reviews .more' ).click( moreReviews );
