const $ = require( 'jquery' );
const modals = require( '../helpers/modals' );
const utils = require( '../helpers/utils' );

if( window.rouletteItems ) {
(function() {
    var requestAnimationFrame = window.requestAnimationFrame || window.mozRequestAnimationFrame ||
                                window.webkitRequestAnimationFrame || window.msRequestAnimationFrame;

    window.requestAnimationFrame = requestAnimationFrame;

})();

function elementWindowPosition( el ) { 
    var position = { x: 0, y: 0 } 
    var rect = el.getBoundingClientRect(); 
    
    position.x = ( rect.left + rect.width / 2 ) / window.innerWidth; 
    position.y = ( rect.top + rect.height / 2 ) / window.innerHeight; 
    
    return position; 
} 
    
function normalize( num ) { 
        
    num = Math.abs( num - .5 ); 
    return ( 1 - num ); 
} 

function range(min, max) { return Math.floor(Math.random() * (max - min)) + min; }

const MAX_CIRCLES = 29;

const Roulette = function() {
    
    if( rouletteItems.length % 2 == 0 ) 
        rouletteItems.push( rouletteItems[ range( 3, rouletteItems.length )] );

    var width = 240;
    var circleWidth = 0;

    var wrap = $( '.roulette-roll__whell' );
    var wheel = $( '.roulette-roll__whell');
    
    var row = rouletteItems.map( function( item )  {

        return '<div class="roll-item" style="background-image: url(' + item.image + ');"></div>'
    }).join( '' );

    wrap.css( {
        left: -wrap.width() / 2 + ( width / 2 ),
        transition: 'none' });

    for(var x = 0; x < MAX_CIRCLES; x++) {

        if( x == 1 )
            circleWidth = wrap.width();

        wheel.html( wheel.html() + row );
    }


    function spinPromise( id ) {
    return new Promise( function (resolve, reject) {

        let index, pixels, circles, pixelsStart;

        var arrayId = rouletteItems.indexOf( rouletteItems.filter( function( item ) { return item.id == id; } )[0] );

        console.log( id, arrayId );

        // pixels = width * id ;
        // circles = circleWidth * 15; //15 circles

        // pixelsStart = pixels;
        // pixels += circles;
        // pixels += width * range( .45, .55 );
        
        // pixels *= -1;
        // pixels = (((pixels * -1) - circles) * -1) + ( wrap.width() / 2);
        
        pixels = width * arrayId;
        circles = circleWidth * 14;
        pixels += circles; // ad 15 circles.
        pixels += width * .5;

        pixels*= -1;

        pixelsStart = ( ( ( pixels * -1 ) - circles ) * -1 ) + ( wrap.width() / 2 );

        wrap.css( { 
            left: pixelsStart,
            transition: 'none' });

        setTimeout( function() {

            console.log( 'START ROLLING' );
            console.log( pixels + wrap.width() / 2, pixelsStart );

            wrap.css( {
                transition: 'left 5s',
                left: pixels + wrap.width() / 2 } );
             
            setTimeout(() => {

                console.log( 'END ROLLING' );

                wrap.css( { 
                    left: pixelsStart,
                    transition: 'none' });

                setTimeout(() => {
                    wrap.css( {
                        transition: 'left 5s' } );

                    resolve();
                }, 510);

            }, 5000 + 700);
        }, 50 );

    });
    }

    function updateAnimation() {

        let items = Array.from( $( '.roll-item' ) );

        items.forEach( function( item ) {

            var pos = normalize( elementWindowPosition( item ).x ); 

            if( pos < 0 ) return;

            item = $( item );

            item.css( {
                transform: 'scale( ' + pos + ' )',
                opacity: pos
            });
        });

        requestAnimationFrame( updateAnimation );
    }

    this.play = function( id ) {

        return spinPromise( id );
    }

    requestAnimationFrame( updateAnimation );
}

var roulette = new Roulette(),
    multiplier = false,
    price = window.price || 59;

$( '#roulette_play' ).click(  function() {

    var data = {
        multiplier: multiplier
    };

    // Disable button.
    $( '#roulette_play' ).prop( 'disabled', true );

    $.ajax( {
        type: 'POST',
        url: '/roulette.roll',
        data: data,
        success: function( result ) {

            if( result.balance ) 
                utils.updateBalance( result.balance )

            if( result.error ) {

                $( '#roulette_play' ).prop( 'disabled', false );

                var transformErrors = {
                    NEED_AUTH:  {
                        title: 'Действие недоступно!',
                        message: 'Чтобы крутить рулетку, нужно авторизоваться на сайте!',
                        buttons: [
                            {
                                text: 'Авторизоваться',
                                primary: true,
                                href: '/auth/login'
                            }
                        ]
                    },
                    

                    LOW_BALANCE: {

                        title: 'Недостаточно денег!',
                        message: 'Увы, но тебе не хватает ' + ( price - result.balance ) + '₽, чтобы выиграть ТОП игру!',
                        buttons: [
                            {
                                text: 'Пополнить баланс',
                                primary: true,
                                href: '/profile'
                            }
                        ]
                    },


                    DEFAULT: {
                        title: 'Неизвестная ошибка!',
                        message: 'Попробуйте ещё раз! Если ошибка повторяется, обратитесь в поддержку'
                    }
                }

                var error = transformErrors[result.error] || transformErrors.DEFAULT;

                return modals.modalError( error.title, error.message, error.buttons  );
            }

            roulette.play( result.game.id ).then( function() {

                // Enable button.
                $( '#roulette_play' ).prop( 'disabled', false );

                modals.modalWin( result.win, result.game )
            });
        }
    });
});

$( '#modalGames' ).click( function() {

    modals.modalGames();
});

$( 'input[name=chance]' ).change( function() {

    if ( $(this).is(':checked') ) {
        
        var value = $( this ).val();

        multiplier = value == 'OFF' ? false : value 

        var data = {
            multiplier: multiplier
        }

        $.ajax( {
            type: 'POST',
            url: '/roulette.price',
            data: data,
            success: function( result ) {

                price = result.price;
                $( '#roulette_play' ).text( 'Крутить за ' + result.price + '₽' );
            }
        });

    }
} );

// $.ajax( {
//     type: 'POST',
//     url: '/roulette.getWin',
//     data: { id: 5 },
//     success: function( result ) {

//         console.log( result );

//         if( result.error ) {

//             $( '#roulette_play' ).prop( 'disabled', false );

//             var transformErrors = {
//                 ACCESS_DENIED:  {
//                     title: 'Доступ ограничен!',
//                     message: 'У вас недостаточно прав для просмотра выигрыша #' + result.id + '!'
//                 },
//                 DEFAULT: {
//                     title: 'Неизвестная ошибка!',
//                     message: 'Попробуйте ещё раз! Если ошибка повторяется, обратитесь в поддержку'
//                 }
//             }

//             var error = transformErrors[result.error] || transformErrors.DEFAULT;

//             return modals.modalError( error.title, error.message, error.buttons  );
//         }

//         modals.modalWin( result.win );
//     }
// });

// setInterval( () => {
//     document.querySelectorAll( '.roll-item' ).forEach( function( item  )  {
//         var pos = normalize( elementWindowPosition( item ).x );

//         item.style = "transform: scale( " + pos + " ); opacity: " + pos + " ;";
//     });
// }, 500)

}