const $ = require( 'jquery' );

setInterval( updateOnline, 10000 );

function renderLives( lives ) {
    
    if( lives.lenght == 0 )
        return;
    console.log( 'render lives' );

    var container = $( '.live-games__container' );
    
    container.html( '' );

    lives = lives.forEach( function( live ) {

        container.append( '<div class="live-games__item" data-_id="' + live._id + '"><img class="live-games__image" src="' + live.game.image + '"><div class="live-games__hint">' + live.user + '</div><div class="live-games__time">' + live.ago + '</div></div>')
    });
    
}

function updateOnline() {

    $.ajax( {
        type: 'POST',
        url: '/online',
        success: function( result ) {

            $( '#onlineStatus' ).text( result.online + ' играют сейчас' );

            renderLives( result.lives );
        }
    });
}

updateOnline();