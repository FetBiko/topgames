const $ = require( 'jquery' );

const updateBalance = function( balance ) {

    $( '.profile-preview__balance' ).text( 'Баланс: ' + balance + '₽' );
}

module.exports = { updateBalance }