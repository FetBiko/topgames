const Tingle = require( 'tingle.js' );
const $ =  require( 'jquery' );
const utils = require( './utils' );

// // instanciate new modal
// var modal = new tingle.modal({
//     footer: true,
//     stickyFooter: false,
//     closeMethods: ['overlay', 'escape'],
//     closeLabel: "Close",
//     cssClass: [ 'modal-s'],
//     onOpen: function() {

//     },
//     onClose: function() {

//     },
//     beforeClose: function() {

//     }
// });

function modalError( title, message, buttons ) {

    if( !buttons ) buttons = [];

    var modal = new Tingle.modal({
        footer: false,
        stickyFooter: false,
        closeMethods: [ 'overlay', 'escape' ],
        closeLabel: "Close",
        cssClass: [ 'modal-s', 'align-center'],

        onClose: function() {
            modal.destroy();
        }
    });

    var buttonsText = '';
    
    if( buttons && buttons.length > 0 ) {
        buttonsText = '<div style="width: 50%; margin: 40px auto 0;">' + buttons.map( function( button ) {
            
            return '<a class="flat-button full-width' + ( button.primary ? ' primary' : ( button.secondary ? ' secondary' : '' ) ) + '" href="' + button.href + '">' + button.text + '</a>';
        }) + '</div>'
    }

    modal.setContent( ( title ? '<h5>' + title + '</h5>' : '' ) + ( message ? '<p>' + message + '<p>' : '' ) + buttonsText );
    modal.open();

    return modal;
}

function modalWin( win, g ) {


    var modal = new Tingle.modal({
        footer: false,
        stickyFooter: false,
        closeMethods: [ 'overlay', 'escape' ],
        closeLabel: "Close",
        cssClass: [ 'modal-m', 'align-center', 'win-modal'],

        onClose: function() {
            modal.destroy();
        }
    });

    modal.render = function( win, g ) {

        let game = g ? g : win.game;

        console.log( game, win, g );

        var info = '<img class="win-modal__image" src="' + game.image + '">';
        var buttons = '';
    
        if( !win.locked ) {
    
            info += '<div class="win-modal__extra"><div class="win-modal__steam-container"><div class="win-modal__steam-icon"></div><div class="win-modal__steam-text">Цена игры в Steam ' + game.steamPrice + '₽</div></div></div>';
            buttons += '<button class="flat-button full-width primary" id="getMoneyPrize">Продать за ' + game.sellPrice + '₽</button>';
            buttons += '<button class="flat-button full-width secondary" id="getKeyPrize">Забрать ключ</button>';
        } else {
    
            info += win.key 
                ? '<div class="win-modal__extra" style="margin-bottom: 40px;"><div class="win-modal__prize">' + win.key + '</div></div>'
                : '<div class="win-modal__extra" style="margin-bottom: 40px;"><div class="win-modal__prize">Игра продана за ' + game.sellPrice + '₽</div></div>'
    
            buttons += '<button class="flat-button full-width primary" id="closeWin">Закрыть</button>';
    
            closeWin = true;
        }
    
    
        modal.setContent( '<h4>Вы выиграли ' + game.title + '</h4>' + info + buttons );
        modal.open();

        $( '#closeWin' ).click( function() {
            modal.close(); } );

        $( '#getMoneyPrize').click( function() {
            
            getPrize( 'money' );
        });

        $( '#getKeyPrize' ).click( function() {

            getPrize( 'key' );
        });

        return modal;
    }
    
    function getPrize( prize ) {

        $.ajax( {
            type: 'POST',
            url: '/roulette.activateWin',
            data: { 
                win: typeof win === 'number' ? win : win.id,
                prize: prize
            },
            success: function( result ) {

                if( result.error ) {
        
                    var transformErrors = {
                        ACCESS_DENIED:  {
                            title: 'Доступ ограничен!',
                            message: 'У вас недостаточно прав для просмотра выигрыша #' + result.id + '!'
                        },
                        UNKNOWN_WIN:  {
                            title: 'Неизвестный выигрыш!',
                            message: 'Мы не смогли найти выигрыш #' + result.id + '!'
                        },
                        DEFAULT: {
                            title: 'Неизвестная ошибка!',
                            message: 'Попробуйте ещё раз! Если ошибка повторяется, обратитесь в поддержку'
                        }
                    }
        
                    var error = transformErrors[result.error] || transformErrors.DEFAULT;
        
                    modalError( error.title, error.message, error.buttons  );
                    return modal.close();
                }

                if( result.balance )
                    utils.updateBalance( result.balance );

                modal.render( result.win );
            }
        });
    }

    return modal.render( win, g );
}

var modalGamesInstance;

function modalGames( games ) {

    if( modalGamesInstance ) {

        modalGamesInstance.open();
        return modalGamesInstance;
    }

    modalGamesInstance = new Tingle.modal({
        footer: true,
        stickyFooter: true,
        closeMethods: [ 'overlay', 'escape' ],
        closeLabel: "Close",
        cssClass: [ 'modal-l', 'align-center', 'modal-games' ]
    });

    modalGamesInstance.render = function( games )  {

        var content = '<h4>Можно выиграть:</h4>';

        var items = games.map( function( game ) {

            var itemContent = '<img class="modal-games__image" src=' + game.image + '>';
                itemContent += '<div class="modal-games__title">' + game.title + '</div>';
                itemContent += '<div class="modal-games__price">' + game.steamPrice + '₽</div>';

            return '<div class="modal-games__item">' + itemContent + '</div>';
        }).join('');

        content += '<div class="modal-games__container">' + items + '</div>';

        modalGamesInstance.setContent( content );
        modalGamesInstance.open();

        return modalGamesInstance;
    }

    $.ajax( {
        type: 'POST',
        url: '/roulette.items',
        data: {},
        success: function( result ) {

            if( result.error ) {
    
                var transformErrors = {
                    DEFAULT: {
                        title: 'Неизвестная ошибка!',
                        message: 'Попробуйте ещё раз! Если ошибка повторяется, обратитесь в поддержку'
                    }
                }
    
                var error = transformErrors[result.error] || transformErrors.DEFAULT;
    
                modalError( error.title, error.message, error.buttons );
                return modal.close();
            }

            modalGamesInstance.render( result.games );
        }
    });

    modalGamesInstance.addFooterBtn( 'Закрыть', 'flat-button primary', function() {

        modalGamesInstance.close();
    });

    modalGamesInstance.open();
    return modalGamesInstance;
}

function modalPayment( content ) {

    let paymentModal = new Tingle.modal({
        footer: false,
        closeMethods: [ 'overlay', 'escape' ],
        closeLabel: "Close",
        cssClass: [ 'modal-s', 'align-center', 'modal-games' ],
        onClose: function() {

            paymentModal.close();
        }
    });

    paymentModal.setContent( content );
    paymentModal.open();
    
    return paymentModal
}

module.exports = { 
    modalError, 
    modalNotification: modalError,
    modalWin,
    modalGames,
    modalPayment
}