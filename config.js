const config = require( 'config' );

const dotenv = require( 'dotenv' );

dotenv.config();

const envs = require( './constants/envs' );
const { ENV } = require( './src/utils/env' );

if( !envs[ENV] ) {
    throw Error( `Unknown environment '${ENV}'` )
}

const PORT = process.env.PORT || config.get( 'port' );
const MONGO_URI = process.env.MONGO_URI || config.get( 'mongo.uri' );

module.exports = {
    PORT,
    MONGO_URI
}