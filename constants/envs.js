const keymirror = require( 'keymirror' );

module.exports = keymirror( {
    production: null,
    development: null
});