
process.on( 'unhandledRejection', console.error )
global.preferences = require( './preferences' );
global.__rootdir = __dirname;

const database = require( './database' );
const { MONGO_URI, PORT } = require( './config' );

database( MONGO_URI )
    .then( () => {

        const app = require( './app' );

        app.listen( PORT, ( error ) => {

            if( error ) return console.error( error );
        
            console.log( `Server running on port ${PORT}` );
        });
        const generateLive = require(  './src/lib/helpers/generate-live' );
        const range = ( min, max ) => Math.random() * ( max - min ) + min;

        const next = () => {

            generateLive( { fake: true });

            var delay =  range( preferences.live.fakeIntervalMin, preferences.live.fakeIntervalMax ) * 1000;

            if( isNaN( delay ) )
                return setTimeout( next, rand( 30, 300 ) * 1000 );

            setTimeout( next, delay );
        }

        next();
    })
    .catch( error => {

        console.error( error );
        throw Error( 'Mongo conenction failed!', error );
    });

