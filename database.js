const mongoose = require( 'mongoose' )
const autoIncrement = require('mongoose-auto-increment');

mongoose.Promise = Promise;

module.exports = ( mongoUri ) => {
    
    if( !mongoUri )     throw Error( 'Mongo url is undefined' );

    return new Promise( ( resolve, reject ) => {

        mongoose
            .connect( mongoUri )
            .then( connection => {

                console.log( 'Mongo connected!' );
                autoIncrement.initialize( connection );

                resolve( connection );
            })
            .catch( reject );

    })
}