var gulp = require('gulp'),
    sass = require('gulp-sass'),
    prefix = require('gulp-autoprefixer'),
    uglify = require('gulp-uglify'),
    browserify = require('browserify'),
    source = require('vinyl-source-stream'),
    buffer = require('vinyl-buffer'),
    cleanCSS = require( 'gulp-clean-css' ),
    rename = require( 'gulp-rename' ),
    merge  = require( 'merge-stream' );


let paths = {
    src: {
        css: './src/public/scss/**/*.scss',
        js: './src/public/js/**/*.js'
    },
    source:  {
        css: './src/public/scss/style.scss',
        js: './src/public/js/script.js'
    },
    dest: {
        css: './public/css/',
        js: './public/js/'
    }
}

let packs = {
    css: [
        {
            file: 'dashboard.css',
            source: './src/public/scss/dashboard.scss',
            dest: './public/css/'
        },
        {
            file: 'common.css',
            source: './src/public/scss/common.scss',
            dest: './public/css/'
        }
    ],
    js: [
        {
            file: 'common.js',
            source: './src/public/js/common.js',
            dest: './public/js/'
        },
        {
            file: 'dashboard.js',
            source: './src/public/js/dashboard.js',
            dest: './public/js/'
        }
    ]
}

/* tasks */
gulp.task('css', () => {

    var tasks = packs.css.map( pack => {
        return gulp.src( pack.source )
            .pipe( sass( { outputStyle: 'compressed' } ) )
            .pipe( prefix( { browsers: ['last 2 versions'], cascade: false } ) )
            .pipe( cleanCSS( { compatibility: 'ie8' } ) )
            .pipe( rename( pack.file ))
            .pipe( gulp.dest( pack.dest || paths.dest.css ) );
    });
    
    return merge( tasks );
});

gulp.task('js', () => {

    var tasks = packs.js.map( pack => {
        return browserify( pack.source ).bundle()
            .pipe( source( pack.source ) )
            .pipe( buffer() )
            // .pipe( uglify() )
            .pipe( rename( pack.file ))
            .pipe( gulp.dest( pack.dest ) )
    });
    
    return merge( tasks );
});

gulp.task('watch', () => {
    gulp.watch( [ paths.src.css ], gulp.parallel( ['css'] ) );
    gulp.watch( [ paths.src.js ], gulp.parallel( ['js'] ) );
});

gulp.task( 'build', gulp.parallel( [ 'css', 'js' ] ) );

gulp.task( 'default', gulp.parallel( [ 'watch' ] ) );
