const Preferences = require( 'preferences' );
const path = require( 'path' );

var DEFAULT = {

    authorization: {
        private_key: 'J86^zl#9w19mKRGZ!WRGd^O2gVF*ed5uKlSmK0IVRyI@7KtBEmGs8j1Xd7HgDaIu',

        path: 'auth/login/vk',

        oauth: {
            client_id: 6817026,
            response_type: 'code',
            scope: [].join( ',' )
        },

        token: {
            client_id: 6817026,
            client_secret: 'C2G3nDrR96RaM6szspxa',
            display: 'popup'
        }
    },
    
    payments: {
        shop: 1877,
        currency: 3, // 3 - рубли.
        secret: '31grgcdndlit6dekzve0n3fo745oukq0x1qrwrm02xpj2ktohyt6qg7ls2kr1wiq',
        remote: [ '109.120.152.109', '145.239.84.249' ],
        description: 'Пополнение баланса'
    },

    onlineTime: 1800000,

    roulette: {

        multipliers: {
            '2': 20,
            '4': 30,
            '8': 50
        },

        defaultMultiplier: 0,

        price: 59
    },

    community: {
        url: '#'
    },

    header: {

        links: [
            { title: 'Условия', href: '/terms' },
            { title: 'Поддержка', href: '/support' },
            { title: 'Отзывы', href: '/reviews' },
            { title: 'Акции', href: '/offers' },
        ]
    },

    decorative: {

        backgroundImage: 'img/background.jpg'
    },

    live: {
        rareChance: .7,
        rarePercent: 25,
        fakeAfterDelay: 30,
        fakeIntervalMin: 30,
        fakeIntervalMax: 300
    }
}

var preferences = new Preferences( 'preferences', DEFAULT, {
    key: 'super-secret-key',
    file: path.join( __dirname, '.preferences'),
    format: 'yaml'
});
 
module.exports = preferences;