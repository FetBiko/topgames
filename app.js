const Koa = require( 'koa' );
const Pug = require( 'koa-pug' );

const serve = require( 'koa-static' );
const logger = require( 'koa-logger' );
const body = require( 'koa-body'  );

const router = require( './src/router' );

const renderer = require( './src/middleware/renderer' );
const errorHandle = require( './src/middleware/error-handler' );


const app = new Koa();

new Pug({
    viewPath: './src/server/views',
    debug: false,
    pretty: true,
    app,
    basedir: './src/server/views',
    locals: { 
        page: 'TOP GAMES',
    }
})

app.use( body() )
app.use( serve( './public', { gzip: true } ) );
app.use( logger() );

app.use( renderer() )
app.use( errorHandle() );

app.use( router.routes() ).use( router.allowedMethods() );

// app.use( )

module.exports = app;